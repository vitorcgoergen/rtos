
/* Activation of User LED


The user LED is connected to Pin PA5. To activate it, we must first enable the clock in the Bus that is connected to the GPIO Ports (APB2).
After that, we must configure the pin mode. Normally there is a GPIOx_Moder register. Not in this case. Here we have two registers,
Port Configuration Register Low and Port Configuration Register High. They are abreviated as GPIOx_CRL and GPIOx_CRH, respectively. 
Since we want to configure the pin 5 of the port A, we will be using the CRL register.

To actually turn the led on, we will be changing, as usual, the Port Output Data Register (GPIOx_ODR) , by writing High or Low in it's pins. 

After creating the program that lights the LED, we then proceed to add a time base to our system using SysTick in order to create a delay function. This delay function
will be used to make the LED Blink.
*/

#include "stm32f10x.h"                  // Device header


volatile uint32_t tick,_tick;  					// Tick of the Clock (Time Base)


// Function's signatures
void init_GPIO(void);
void SysTick_Handler(void);
uint32_t getTick(void);	
void Delay(uint32_t time);

int main() {
	init_GPIO();
	while(1) {
	Delay(1);
	GPIOA->ODR = (GPIOA->ODR & 0x0) | GPIO_ODR_ODR5;
	Delay(1);
	GPIOA->ODR = (GPIOA->ODR & 0x0) & ~GPIO_ODR_ODR5;
	}

}





// GPIO Initialisation Function
void init_GPIO(void) {
	
	// Perihperal Initialisation
	RCC->APB2ENR = (RCC->APB2ENR & 0x0) | RCC_APB2ENR_IOPAEN;											//  Initialise clock of APB2 Bus
	GPIOA->CRL &= ~GPIO_CRL_CNF5;																									//  Put the PA in General Purpose output Push pull
	GPIOA->CRL = (GPIOA->CRL & 0x44444444) | GPIO_CRL_MODE5_1;										//	Put the port in output mode

	// Time configuration 
	SystemCoreClockUpdate(); 																											//	Update the System's clock
	SysTick_Config(SystemCoreClock/100U);																					//	Configure SysTick using the Core Clock
	__enable_irq();																																//	Enable IRQ, since SysTick is based on exceptions
	
}


// Incrementation of the Clock Tick in an Interruption Handler
void SysTick_Handler(void) {
	tick ++;
}


// Function to store a given tick value inside an auxiliary variable
uint32_t getTick(void) {
	
	__disable_irq();
	_tick = tick;																																	// Stores the value. This line must be executed 'atomically', hence the need to disable the IRQ before executing it 
	__enable_irq();
	
	return _tick;
	
}

// Delay Function. The argument time is in seconds.
void Delay(uint32_t time) {
		time *= 100;																																// Multiplication necessary so that the given time will be waited in seconds
		uint32_t FixedTime = getTick();																							// Get a time and fix it to start counting
		while((getTick() - FixedTime) < time) {}																		// Keep counting until reaching the limit
}

